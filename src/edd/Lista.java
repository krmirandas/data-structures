
package edd;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Comparator;

/**
 * <p>Clase genérica para listas doblemente ligadas.</p>
 *
 * <p>Las listas nos permiten agregar elementos al inicio o final de la lista,
 * eliminar elementos de la lista, comprobar si un elemento está o no en la
 * lista, y otras operaciones básicas.</p>
 *
 * <p>Las listas implementan la interfaz {@link Iterable}, y por lo tanto se
 * pueden recorrer usando la estructura de control <em>for-each</em>. Las listas
 * no aceptan a <code>null</code> como elemento.</p>
 */
public class Lista<T> implements Coleccion<T> {

    /* Clase Nodo privada para uso interno de la clase Lista. */
    private class Nodo {
        /* El elemento del nodo. */
        public T elemento;
        /* El nodo anterior. */
        public Nodo anterior;
        /* El nodo siguiente. */
        public Nodo siguiente;

        /* Construye un nodo con un elemento. */
        public Nodo(T elemento) {
            this.elemento = elemento;// Aquí va su código.
        }
    }

    /* Clase Iterador privada para iteradores. */
    private class Iterador implements IteradorLista<T> {
        /* El nodo anterior. */
        public Nodo anterior;
        /* El nodo siguiente. */
        public Nodo siguiente;

        /* Construye un nuevo iterador. */
        public Iterador() {
            siguiente = cabeza;// Aquí va su código.
        }

        /* Nos dice si hay un elemento siguiente. */
        @Override public boolean hasNext() {
            return siguiente!=null;// Aquí va su código.
        }

        /* Nos da el elemento siguiente. */
        @Override public T next() {
            if(siguiente==null){
                throw new NoSuchElementException();
            }
            anterior = siguiente;
            siguiente = siguiente.siguiente;
            return anterior.elemento;// Aquí va su código.
        }

        /* Nos dice si hay un elemento anterior. */
        @Override public boolean hasPrevious() {
            return anterior!=null;// Aquí va su código.
        }

        /* Nos da el elemento anterior. */
        @Override public T previous() {
            if(anterior==null){
                throw new NoSuchElementException();
            }
            siguiente = anterior;
            anterior = anterior.anterior;
            return siguiente.elemento;// Aquí va su código.
        }

        /* Mueve el iterador al inicio de la lista. */
        @Override public void start() {
            anterior= null;
            siguiente = cabeza;// Aquí va su código.
        }

        /* Mueve el iterador al final de la lista. */
        @Override public void end() {
            anterior = rabo;
            siguiente = null;// Aquí va su código.
        }

    }

    /* Primer elemento de la lista. */
    private Nodo cabeza;
    /* Último elemento de la lista. */
    private Nodo rabo;
    /* Número de elementos en la lista. */
    private int longitud;

    /**
     * Regresa la longitud de la lista. El método es idéntico a {@link
     * #getElementos}.
     * @return la longitud de la lista, el número de elementos que contiene.
     */
    public int getLongitud() {
        return longitud;// Aquí va su código.
    }

    /**
     * Regresa el número elementos en la lista. El método es idéntico a {@link
     * #getLongitud}.
     * @return el número elementos en la lista.
     */
    @Override public int getElementos() {
        return longitud;// Aquí va su código.
    }

    /**
     * Nos dice si la lista es vacía.
     * @return <code>true</code> si la lista es vacía, <code>false</code> en
     *         otro caso.
     */
    @Override public boolean esVacio() {
        if(cabeza==null)
        return true;
        return false;    // Aquí va su código.
    }

    /**
     * Agrega un elemento a la lista. Si la lista no tiene elementos, el
     * elemento a agregar será el primero y último. El método es idéntico a
     * {@link #agregaFinal}.
     * @param elemento el elemento a agregar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    @Override public void agrega(T elemento) {
        Nodo nuevo = new Nodo(elemento);
        if (elemento ==null){
            throw new IllegalArgumentException ("Elemento nulo");
        }else{
            if(rabo == null){//si no tiene elementos
        nuevo.siguiente = null;
        nuevo.anterior = null;
        cabeza = rabo = nuevo;

        }else{
            rabo.siguiente = nuevo;
            nuevo.anterior= rabo;
            rabo = nuevo;
             nuevo.siguiente = null;

         }

        }
        
        longitud ++;
    }

    /**
     * Agrega un elemento al final de la lista. Si la lista no tiene elementos,
     * el elemento a agregar será el primero y último.
     * @param elemento el elemento a agregar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    public void agregaFinal(T elemento) {
        Nodo nuevo = new Nodo(elemento);
        if (elemento ==null){
            throw new IllegalArgumentException ("Elemento nulo");
        }else{
            if(rabo == null){//si no tiene elementos
        nuevo.siguiente = null;
        nuevo.anterior = null;
        cabeza = rabo = nuevo;

        }else{
            rabo.siguiente = nuevo;
            nuevo.anterior= rabo;
            rabo = nuevo;
             nuevo.siguiente = null;

         }

        }
        
        longitud ++;
    }

    /**
     * Agrega un elemento al inicio de la lista. Si la lista no tiene elementos,
     * el elemento a agregar será el primero y último.
     * @param elemento el elemento a agregar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    public void agregaInicio(T elemento) {
        if(elemento==null)
            throw new IllegalArgumentException("Elemento es nulo");
        Nodo nuevo = new Nodo(elemento);
        if(esVacio()){
            nuevo.siguiente = null;
            nuevo.anterior = null;
            cabeza=rabo=nuevo;
        }else{
            nuevo.anterior = null;
            nuevo.siguiente = cabeza;
            cabeza = nuevo;
            cabeza.siguiente.anterior = cabeza;
        } 
        longitud++;   // Aquí va su código.
    }

    /**
     * Inserta un elemento en un índice explícito.
     *
     * Si el índice es menor que cero, el elemento se agrega al inicio de la
     * lista. Si el índice es mayor o igual que el número de elementos en la
     * lista, el elemento se agrega al fina de la misma. En otro caso, después
     * de mandar llamar el método, el elemento tendrá el índice que se
     * especifica en la lista.
     * @param i el índice dónde insertar el elemento. Si es menor que 0 el
     *          elemento se agrega al final, y si es mayor o igual que el número
     *          de elementos en la lista se agrega al inicio.
     * @param elemento el elemento a insertar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    public void inserta(int i, T elemento) {
        if(elemento==null){
            throw new IllegalArgumentException("Elemento es nulo");
        }
        else
        if(i<=0){
            agregaInicio(elemento);
        }

        else
            if(i>=longitud){
            agregaFinal(elemento);

            }

            else{
             Nodo nuevo = new Nodo(elemento);
            Nodo auxiliar = cabeza;
               
        for (int j=0;j<i;j++){ //complejidad 2n+2
            auxiliar = auxiliar.siguiente;
        }
        auxiliar.anterior.siguiente = nuevo;
        nuevo.anterior = auxiliar.anterior;
         auxiliar.anterior = nuevo;
         nuevo.siguiente = auxiliar;
        
          longitud++;
         
            }
           
        
        }
            
    /**
     * Elimina un elemento de la lista. Si el elemento no está contenido en la
     * lista, el método no la modifica.
     * @param elemento el elemento a eliminar.
     */
    @Override public void elimina(T elemento) {
    Nodo aux = cabeza;
        if (esVacio())
        return;
    while(aux !=null){
        if (aux.elemento.equals(elemento)){
            if(cabeza.equals(rabo)){
               cabeza=null;
               rabo=cabeza;
            }
            else 
                if(aux == cabeza){
                cabeza = cabeza.siguiente;
               cabeza.anterior = null;
    
            }
            else 
                if(aux == rabo){
                   rabo = rabo.anterior;
                    rabo.siguiente = null;
            }
            else
            {
                aux.anterior.siguiente=aux.siguiente;
                aux.siguiente.anterior=aux.anterior;
                
            }
        }
        aux=aux.siguiente; //Avanza el siguiente mientras compara.
    }
    longitud--;
    return;
    }

    /**
     * Elimina el primer elemento de la lista y lo regresa.
     * @return el primer elemento de la lista antes de eliminarlo.
     * @throws NoSuchElementException si la lista es vacía.
     */
    public T eliminaPrimero() {
          if (cabeza ==null )
            throw new NoSuchElementException ("La lista es vacia ");
              T elem = getPrimero();

        if (esVacio()){
            return null;
        }
        else 
            if (rabo.equals(rabo)){
                limpia(); 
        }
        else
        {
            cabeza = cabeza.siguiente;
               cabeza.anterior = null;
                  longitud--;
        }
        return elem;
    }

    /**
     * Elimina el último elemento de la lista y lo regresa.
     * @return el último elemento de la lista antes de eliminarlo.
     * @throws NoSuchElementException si la lista es vacía.
     */
    public T eliminaUltimo() {   
        if(esVacio())
        throw new NoSuchElementException ("La lista es vacia");
       T elem =getUltimo();
      if (rabo==null){
        return null;
        }
        else if (cabeza.equals(rabo)){
            limpia();
        }
        else{
            rabo = rabo.anterior;
             rabo.siguiente = null;
             longitud--;
        }
        return elem;
    }

    /**
     * Nos dice si un elemento está en la lista.
     * @param elemento el elemento que queremos saber si está en la lista.
     * @return <tt>true</tt> si <tt>elemento</tt> está en la lista,
     *         <tt>false</tt> en otro caso.
     */
    @Override public boolean contiene(T elemento) {
        Nodo aux = cabeza;
        while ( aux != null) {
            if (aux.elemento.equals(elemento))
                return true;
            aux = aux.siguiente;
       }
        return false;    
    }

    /**
     * Regresa la reversa de la lista.
     * @return una nueva lista que es la reversa la que manda llamar el método.
     */
    public Lista<T> reversa() {
       Lista<T> lista = new Lista<T>();
        Nodo aux = cabeza;
        while (aux != null) {
            lista.agregaInicio(aux.elemento);
            aux = aux.siguiente;
        }
        return lista;
    }

    /**
     * Regresa una copia de la lista. La copia tiene los mismos elementos que la
     * lista que manda llamar el método, en el mismo orden.
     * @return una copiad de la lista.
     */
    public Lista<T> copia() {
        Lista<T> lista = new Lista<T>(); /**Se crea a lista y un axiliar que recorrera toda la lista hasta ya no
                                         haya elementos, empieza a agregar desde el final de modo que la cabeza quede al principio*/
        Nodo aux = cabeza;
        while (aux != null) {
            lista.agregaFinal(aux.elemento);
             aux = aux.siguiente;
        }
        return lista;// Aquí va su código.
    }

    /**
     * Limpia la lista de elementos, dejándola vacía.
     */
    @Override public void limpia() {
        cabeza=rabo=null;
        longitud= 0;
    }

    /**
     * Regresa el primer elemento de la lista.
     * @return el primer elemento de la lista.
     * @throws NoSuchElementException si la lista es vacía.
     */
    public T getPrimero() {
        if (esVacio()){
            throw new NoSuchElementException();
    }
    else
    return cabeza.elemento; 
            
        
    }

    /**
     * Regresa el último elemento de la lista.
     * @return el primer elemento de la lista.
     * @throws NoSuchElementException si la lista es vacía.
     */
    public T getUltimo() {
        if (cabeza == null)
            throw new NoSuchElementException ("la lista es vacía "); 
        else
            return rabo.elemento;
    }

    /**
     * Regresa el <em>i</em>-ésimo elemento de la lista.
     * @param i el índice del elemento que queremos.
     * @return el <em>i</em>-ésimo elemento de la lista.
     * @throws ExcepcionIndiceInvalido si <em>i</em> es menor que cero o mayor o
     *         igual que el número de elementos en la lista.
     */
    public T get(int i) {
       if (i<0 || longitud <=i)
     throw new ExcepcionIndiceInvalido ("No");
        Nodo aux = cabeza;
        for(int cont = 0; cont < i; cont++){ //Complejidad 2n+2
            aux = aux.siguiente;
        }
        return aux.elemento;
    }
    

    /**
     * Regresa el índice del elemento recibido en la lista.
     * @param elemento el elemento del que se busca el índice.
     * @return el índice del elemento recibido en la lista, o -1 si
     *         el elemento no está contenido en la lista.
     */
    public int indiceDe(T elemento) {
        Nodo auxiliar =cabeza;
        for(int i=0; auxiliar != null ;i++){  //complejidad 1 asignacion + (n+1)comparaciones + n incrementos
            if (auxiliar.elemento.equals(elemento)){
                return i;
            }
         auxiliar = auxiliar.siguiente;
        }
        return -1;
        }

    /**
     * Regresa una representación en cadena de la lista.
     * @return una representación en cadena de la lista.
     */
    @Override public String toString() {
        if (cabeza==null)
            
            return "[]";
        
        String s = "[";
        
        for (int i = 0; i < longitud-1; i++)
           
            s += String.format("%s, ", get(i));

        s += String.format("%s]", get(longitud-1));

        return s;
    }

    /**
     * Nos dice si la lista es igual al objeto recibido.
     * @param o el objeto con el que hay que comparar.
     * @return <tt>true</tt> si la lista es igual al objeto recibido;
     *         <tt>false</tt> en otro caso.
     */
    @Override public boolean equals(Object o) {
             if (o == null) 
            return false;
        if (o instanceof Lista){
        @SuppressWarnings("unchecked") Lista<T> lista = (Lista<T>)o;
        if (lista.getLongitud() !=longitud)
                return false;
            Nodo auxiliar =cabeza;
            Nodo aux = lista.cabeza;
            while(auxiliar !=null && aux !=null){
                if(!auxiliar.elemento.equals(aux.elemento)) 
                    return false;
                auxiliar =auxiliar.siguiente;
                aux =aux.siguiente;
        }
        return true;
    }
    return false;
    }
    /**
     * Regresa un iterador para recorrer la lista en una dirección.
     * @return un iterador para recorrer la lista en una dirección.
     */
    @Override public Iterator<T> iterator() {
        return new Iterador();
    }

    /**
     * Regresa un iterador para recorrer la lista en ambas direcciones.
     * @return un iterador para recorrer la lista en ambas direcciones.
     */
    public IteradorLista<T> iteradorLista() {
        return new Iterador();
    }

     /**
     * Regresa una copia de la lista, pero ordenada. Para poder hacer el
     * ordenamiento, el método necesita una instancia de {@link Comparator} para
     * poder comparar los elementos de la lista.
     * @param comparador el comparador que la lista usará para hacer el
     *                   ordenamiento.
     * @return una copia de la lista, pero ordenada.
     */
    public Lista<T> mergeSort(Comparator<T> comparador) {
         if (longitud <=1)
        return copia();
      Lista<T> izq = new Lista<T>();
      Lista<T> der = new Lista<T>();
       Lista<T> l = new Lista<T>();
      int contador = 0;
       for(T e: this){  /*Para cada elemento del tipo T que se encuentre dentro de la colección 
         ejecuta las instrucciones que se indican*/
            if(contador < longitud/2 )
                l = izq;
            else
                l = der;

            l.agregaFinal(e);
            contador++;

      }
      izq = izq.mergeSort(comparador);
      der = der.mergeSort(comparador);
      return izq.mezcla(der, comparador);
    }

    private Lista<T> mezcla (Lista<T> laux, Comparator<T> comparador){
      Lista<T> lista = new Lista<T>();
      Nodo izq = this.cabeza;
      Nodo der = laux.cabeza;
      while (izq != null && der != null){
      if (comparador.compare(izq.elemento, der.elemento)<0){ //si el apuntador izquiera es menor lo agrega al final
         lista.agregaFinal(izq.elemento);
         izq = izq.siguiente; //recorremos la lista
    }else{ // si der es mayor
          lista.agregaFinal(der.elemento);
          der = der.siguiente;
         }
       }
       Nodo nuevo;
       if(izq ==null)
        nuevo = der;
    else
        nuevo = izq;

      while (nuevo!= null){
          lista.agregaFinal(nuevo.elemento);
          nuevo = nuevo.siguiente;
      }
         return lista;
       } 
    

     /**
     * Regresa una copia de la lista recibida, pero ordenada. La lista recibida
     * tiene que contener nada más elementos que implementan la interfaz {@link
     * Comparable}.
     * @param <T> tipo del que puede ser la lista.
     * @param lista la lista que se ordenará.
     * @return una copia de la lista recibida, pero ordenada.
     */
    public static <T extends Comparable<T>>
    Lista<T> mergeSort(Lista<T> lista) {
        return lista.mergeSort((a, b) -> a.compareTo(b));
    }
     /**
     * Busca un elemento en la lista ordenada, usando el comparador recibido. El
     * método supone que la lista está ordenada usando el mismo comparador.
     * @param elemento el elemento a buscar.
     * @param comparador el comparador con el que la lista está ordenada.
     * @return <tt>true</tt> si elemento está contenido en la lista,
     *         <tt>false</tt> en otro caso.
     */
    public boolean busquedaLineal(T elemento, Comparator<T> comparador) {
         Iterador t = new Iterador();
        if(!t.hasNext()){
            return false;
        }
        for(T elem:this){ /*Para cada elemento del tipo T que se encuentre dentro de la colección 
         ejecuta las instrucciones que se indican*/
            if(comparador.compare(elemento,t.next())==0)  
                //elemento.compareTo(t.next)
                return true;
           
        }
        return false;
    }
     /**
     * Busca un elemento en una lista ordenada. La lista recibida tiene que
     * contener nada más elementos que implementan la interfaz {@link
     * Comparable}, y se da por hecho que está ordenada.
     * @param <T> tipo del que puede ser la lista.
     * @param lista la lista donde se buscará.
     * @param elemento el elemento a buscar.
     * @return <tt>true</tt> si el elemento está contenido en la lista,
     *         <tt>false</tt> en otro caso.
     */
    public static <T extends Comparable<T>>
    boolean busquedaLineal(Lista<T> lista, T elemento) {
        return lista.busquedaLineal(elemento, (a, b) -> a.compareTo(b));
    }
}
