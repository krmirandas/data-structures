package edd;

import java.util.Comparator;

/**
 * Clase para ordenar y buscar arreglos genéricos.
  */
public class Arreglos {

    /**
     * Ordena el arreglo recibido usando QickSort.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo el arreglo a ordenar.
     * @param comparador el comparador para ordenar el arreglo.
     */
    public static <T> void
    quickSort(T[] arreglo, Comparator<T> comparador){
         quickSort(arreglo,comparador, 0, arreglo.length-1);
        
    }

   private static <T> void quickSort(T[] arreglo, Comparator<T> comparador, int primero, int ultimo){
    if(ultimo <= primero) return;
    int i = primero + 1;
    int j = ultimo;
   while(i < j)
     if(comparador.compare(arreglo[i], arreglo[primero] )> 0 && comparador.compare(arreglo[j],arreglo[primero])<=0)
       intercambia(arreglo, comparador, i++, j--);
     else if(comparador.compare(arreglo[i], arreglo[primero])<= 0)
       i++;
     else 
       j--;
   if(comparador.compare(arreglo[i], arreglo[primero]) > 0)
      i--;
   intercambia(arreglo, comparador, i, primero);
   quickSort(arreglo, comparador, primero, i-1);
   quickSort(arreglo, comparador, i + 1, ultimo);
  }



    private static <T> void intercambia(T[] arreglo, int i, int j){
         T aux = arreglo[i];
         arreglo[i] = arreglo[j];
         arreglo[j] = aux;
    }  

    private static <T> void intercambia(T[] arreglo, Comparator<T> comparador, int i, int j){
    T aux = arreglo[i];
    arreglo[i] = arreglo[j];
    arreglo[j] = aux;
  }

  private static <T extends Comparable<T>> void quickSort(T[]arreglo, int primero, int ultimo){
        if(ultimo <= primero) return;
        int i = primero+1;
        int j = ultimo;
        while(i < j)
        if(arreglo[i].compareTo(arreglo[primero] )> 0 && arreglo[j].compareTo(arreglo[primero])<= 0)  
          intercambia(arreglo, i++, j--);
          else if(arreglo[i].compareTo(arreglo[primero]) <= 0)
            i++;
          else 
            j--;
         if(arreglo[i].compareTo(arreglo[primero]) > 0)
           i--;
           intercambia(arreglo, i, primero);
           quickSort(arreglo, primero, i-1);
           quickSort(arreglo, i+1, ultimo);  
    }

    /**
     * Ordena el arreglo recibido usando QickSort.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo un arreglo cuyos elementos son comparables.
     */
    public static <T extends Comparable<T>> void
    quickSort(T[] arreglo) {
        quickSort(arreglo, (a, b) -> a.compareTo(b));
    }

    /**
     * Ordena el arreglo recibido usando SelectionSort.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo el arreglo a ordenar.
     * @param comparador el comparador para ordernar el arreglo.
     */
    public static <T> void
    selectionSort(T[] arreglo, Comparator<T> comparador) {
        for(int i=0; i< arreglo.length -1; i++){
           int minimo = i; //valor mas pequeño
           for (int j = i+1; j < arreglo.length; j++){
               if(comparador.compare(arreglo[j], arreglo[minimo])< 0){
                  minimo = j; // si sí ahora sigue comparando desde la nueva posicion
               }
        }

         T aux = arreglo[i];
         arreglo[i] = arreglo[minimo]; //hacemos el intercambio de posiciones
         arreglo[minimo] = aux;
    }
}

    /**
     * Ordena el arreglo recibido usando SelectionSort.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo un arreglo cuyos elementos son comparables.
     */
    public static <T extends Comparable<T>> void
    selectionSort(T[] arreglo) {
         selectionSort(arreglo, (a, b) -> a.compareTo(b));
       
    }

    /**
     * Hace una búsqueda binaria del elemento en el arreglo. Regresa el índice
     * del elemento en el arreglo, o -1 si no se encuentra.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo el arreglo dónde buscar.
     * @param elemento el elemento a buscar.
     * @param comparador el comparador para hacer la búsqueda.
     * @return el índice del elemento en el arreglo, o -1 si no se encuentra.
     */
    public static <T> int
    busquedaBinaria(T[] arreglo, T elemento, Comparator<T> comparador) {
        int bajo = 0;
        int alto =arreglo.length -1;
        while(bajo <= alto){
            int centro = (bajo + alto)/2; //indice elemento central
            if(comparador.compare(elemento, arreglo[centro]) == 0) //si son iguales regresa el indice del centro
                return centro;
            else if(comparador.compare(elemento, arreglo[centro])<0){ //si es menor buscara en los menores que el elemento que buscas
                alto = centro -1; /*ejemplo [2,3,4,5,6,7,20,51] elemento buscado= 51 centro = 3 = elem(5)
                entonces bajo = 4 y centro = 6 = elem(20), luego bajo = 7 y centro = 7 y como es igual lo devuelve*/
            }else 
                bajo = centro +1;
        }
        return -1;
      
    }

    /**
     * Hace una búsqueda binaria del elemento en el arreglo. Regresa el índice
     * del elemento en el arreglo, o -1 si no se encuentra.
     * @param <T> tipo del que puede ser el arreglo.
     * @param arreglo un arreglo cuyos elementos son comparables.
     * @param elemento el elemento a buscar.
     * @return el índice del elemento en el arreglo, o -1 si no se encuentra.
     */
    public static <T extends Comparable<T>> int
    busquedaBinaria(T[] arreglo, T elemento) {
       return busquedaBinaria(arreglo, elemento, (a, b) -> a.compareTo(b));
    }
}
