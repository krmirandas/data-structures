package edd;

import java.util.Iterator;

/**
 * <p>Clase para árboles binarios ordenados. Los árboles son genéricos, pero
 * acotados a la interfaz {@link Comparable}.</p>
 *
 * <p>Un árbol instancia de esta clase siempre cumple que:</p>
 * <ul>
 *   <li>Cualquier elemento en el árbol es mayor o igual que todos sus
 *       descendientes por la izquierda.</li>
 *   <li>Cualquier elemento en el árbol es menor o igual que todos sus
 *       descendientes por la derecha.</li>
 * </ul>
 */
public class ArbolBinarioOrdenado<T extends Comparable<T>>
    extends ArbolBinario<T> {

    /* Clase privada para iteradores de árboles binarios ordenados. */
    private class Iterador implements Iterator<T> {

        /* Pila para recorrer los vértices en DFS in-order. */
        private Pila<Vertice> pila;

        /* Construye un iterador con el vértice recibido. */
        public Iterador() {
           pila = new Pila<ArbolBinario<T>.Vertice>();
            if (esVacio()) {
                return;
            }
            Vertice vi = raiz;
            while(vi != null){
                pila.mete(vi);
                vi = vi.izquierdo;
            }
        }

        /* Nos dice si hay un elemento siguiente. */
        @Override public boolean hasNext() {
            return !pila.esVacia();
        }

        /* Regresa el siguiente elemento en orden DFS in-order. */
        @Override public T next() {
           Vertice v = pila.saca(), vi;
            if (v.hayDerecho()) {
                vi = v.derecho;
                while(vi != null){
                    pila.mete(vi);
                    vi = vi.izquierdo;
                }
            }
            return v.elemento;
        }
    }

    /**
     * El vértice del último elemento agegado. Este vértice sólo se puede
     * garantizar que existe <em>inmediatamente</em> después de haber agregado
     * un elemento al árbol. Si cualquier operación distinta a agregar sobre el
     * árbol se ejecuta después de haber agregado un elemento, el estado de esta
     * variable es indefinido.
     */
    protected Vertice ultimoAgregado;

    /**
     * Constructor sin parámetros. Para no perder el constructor sin parámetros
     * de {@link ArbolBinario}.
     */
    public ArbolBinarioOrdenado() { super(); }

    /**
     * Construye un árbol binario ordenado a partir de una colección. El árbol
     * binario ordenado tiene los mismos elementos que la colección recibida.
     * @param coleccion la colección a partir de la cual creamos el árbol
     *        binario ordenado.
     */
    public ArbolBinarioOrdenado(Coleccion<T> coleccion) {
        super(coleccion);
    }

     private void agrega(Vertice v, T elemento) {
        if (elemento.compareTo(v.elemento) <= 0) {
            if (!v.hayIzquierdo()) {
                v.izquierdo = new Vertice(elemento);
                v.izquierdo.padre = v;
                this.ultimoAgregado = v.izquierdo;
                this.elementos++;
                return;
            }
            this.agrega(v.izquierdo, elemento);
        } else {
            if (!v.hayDerecho()) {
                v.derecho = new Vertice(elemento);
                v.derecho.padre = v;
                this.ultimoAgregado = v.derecho;
                this.elementos++;
                return;
            }
            this.agrega(v.derecho, elemento);
        }
    }

    /**
     * Agrega un nuevo elemento al árbol. El árbol conserva su orden in-order.
     * @param elemento el elemento a agregar.
     */
    @Override public void agrega(T elemento) {
         if (elemento == null) {
           throw new IllegalArgumentException();
       }
        if (this.esVacio()) {
            this.raiz = this.ultimoAgregado = new Vertice(elemento);
            this.elementos++;
        } else {
            this.agrega(this.raiz,  elemento);
        }
    }

    /**
     * Elimina un elemento. Si el elemento no está en el árbol, no hace nada; si
     * está varias veces, elimina el primero que encuentre (in-order). El árbol
     * conserva su orden in-order.
     * @param elemento el elemento a eliminar.
     */
   @Override public void elimina(T elemento){

     Vertice eliminado = (Vertice)busca(elemento);
        if(eliminado == null){
            return;
        }
         if (eliminado.hayIzquierdo()) {
            
            eliminado = intercambiaEliminable(eliminado.izquierdo);
        }
        if(!eliminado.hayIzquierdo() && !eliminado.hayDerecho()){
            if (this.raiz == eliminado) {
            this.raiz = null;
            this.ultimoAgregado = null;
        } else if (eliminado.padre.izquierdo == eliminado) {
            eliminado.padre.izquierdo = null;
        } else {
            eliminado.padre.derecho = null;
        }
        this.elementos--;
        }
        if(!eliminado.hayIzquierdo() || !eliminado.hayDerecho()){
            this.eliminaVertice(eliminado);
        }

   }


    /**
     * Intercambia el elemento de un vértice con dos hijos distintos de
     * <code>null</code> con el elemento de un descendiente que tenga a lo más
     * un hijo.
     * @param vertice un vértice con dos hijos distintos de <code>null</code>.
     * @return el vértice descendiente con el que vértice recibido se
     *         intercambió. El vértice regresado tiene a lo más un hijo distinto
     *         de <code>null</code>.
     */
    protected Vertice intercambiaEliminable(Vertice vertice) { 

    Vertice iz = vertice;
        T iq = vertice.elemento;
        while(vertice.hayDerecho()) {
            vertice = vertice.derecho;
        }
        iz.elemento = vertice.elemento;
        vertice.elemento = iq;
        return vertice;
    }
    /**
     * Elimina un vértice que a lo más tiene un hijo distinto de
     * <code>null</code> subiendo ese hijo (si existe).
     * @param vertice el vértice a eliminar; debe tener a lo más un hijo
     *                distinto de <code>null</code>.
     */
        protected void eliminaVertice(Vertice vertice){
            if(!vertice.hayIzquierdo()){
            if (raiz == vertice) {
            raiz = raiz.derecho;
            vertice.derecho.padre = null;
            } else {
            vertice.derecho.padre = vertice.padre;
            if (vertice.padre.izquierdo == vertice) {
                vertice.padre.izquierdo = vertice.derecho;
            } else {
                vertice.padre.derecho = vertice.derecho;
            }
          }
         elementos--;
         }
          if(!vertice.hayDerecho()){
            if (raiz == vertice) {
            raiz = raiz.izquierdo;
            vertice.izquierdo.padre = null;
            } else {
            vertice.izquierdo.padre = vertice.padre;
            if (vertice.padre.izquierdo == vertice) {
                vertice.padre.izquierdo = vertice.izquierdo;
            } else {
                vertice.padre.derecho = vertice.izquierdo;
            }
            }
         elementos--;
        }
    }
    /**
     * Busca un elemento en el árbol recorriéndolo in-order. Si lo encuentra,
     * regresa el vértice que lo contiene; si no, regresa <tt>null</tt>.
     * @param elemento el elemento a buscar.
     * @return un vértice que contiene al elemento buscado si lo
     *         encuentra; <tt>null</tt> en otro caso.
     */
    @Override public VerticeArbolBinario<T> busca(T elemento) {
        return buscar(raiz, elemento);
    }
   protected Vertice buscar(Vertice vertice, T elemento) {
        Vertice v;
        if (vertice == null) {
            return null;
        }
        v = buscar(vertice.izquierdo, elemento);
        if (v != null) {
            return v;
        }
        if (vertice.elemento.equals(elemento)) {
            return vertice;
        }
        return buscar(vertice.derecho, elemento);
    }


    /**
     * Regresa el vértice que contiene el último elemento agregado al
     * árbol. Este método sólo se puede garantizar que funcione
     * <em>inmediatamente</em> después de haber invocado al método {@link
     * agrega}. Si cualquier operación distinta a agregar sobre el árbol se
     * ejecuta después de haber agregado un elemento, el comportamiento de este
     * método es indefinido.
     * @return el vértice que contiene el último elemento agregado al árbol, si
     *         el método es invocado inmediatamente después de agregar un
     *         elemento al árbol.
     */
    public VerticeArbolBinario<T> getUltimoVerticeAgregado() {
        return ultimoAgregado;
    }

    /**
     * Gira el árbol a la derecha sobre el vértice recibido. Si el vértice no
     * tiene hijo izquierdo, el método no hace nada.
     * @param vertice el vértice sobre el que vamos a girar.
     */
    public void giraDerecha(VerticeArbolBinario<T> vertice) {
       if (vertice == null || !vertice.hayIzquierdo()) {
            return;
        }
        Vertice v = this.vertice(vertice);
        Vertice vi = v.izquierdo;
        vi.padre = v.padre;
        if (v != this.raiz) {
            if (this.esHijoIzquierdo(v)) {
                vi.padre.izquierdo = vi;
            } else {
                vi.padre.derecho = vi;
            }
        }
        v.izquierdo = vi.derecho;
        if (vi.hayDerecho()) {
            vi.derecho.padre = v;
        }
        v.padre = vi;
        vi.derecho = v;
    }

    /**
     * Gira el árbol a la izquierda sobre el vértice recibido. Si el vértice no
     * tiene hijo derecho, el método no hace nada.
     * @param vertice el vértice sobre el que vamos a girar.
     */
    public void giraIzquierda(VerticeArbolBinario<T> vertice) {
       if (vertice == null || !vertice.hayDerecho()) {
            return;
        }
        Vertice v = this.vertice(vertice);
        Vertice vd = v.derecho;
        vd.padre = v.padre;
        if (v != this.raiz) {
            if (this.esHijoIzquierdo(v)) {
                vd.padre.izquierdo = vd;
            } else {
                vd.padre.derecho = vd;
            }
        }
        v.derecho = vd.izquierdo;
        if (vd.hayIzquierdo()) {
            vd.izquierdo.padre = v;
        }
        v.padre = vd;
        vd.izquierdo = v;
    }

    private boolean esHijoIzquierdo(Vertice v) {
        if (!v.hayPadre()) {
            return false;
        }
        return v.padre.izquierdo == v;
    }

    /**
     * Realiza un recorrido DFS <em>pre-order</em> en el árbol, ejecutando la
     * acción recibida en cada elemento del árbol.
     * @param accion la acción a realizar en cada elemento del árbol.
     */
      public void dfsPreOrder(AccionVerticeArbolBinario<T> accion) {
        dfsPreOrderaux(accion, raiz);


    }
    private void dfsPreOrderaux(AccionVerticeArbolBinario<T> accion , Vertice a){
        if(a==null)
            return;
        if(a!=null){
            accion.actua(a);
            dfsPreOrderaux(accion, a.izquierdo);
            dfsPreOrderaux(accion, a.derecho);
        }

    }

    /**
     * Realiza un recorrido DFS <em>in-order</em> en el árbol, ejecutando la
     * acción recibida en cada elemento del árbol.
     * @param accion la acción a realizar en cada elemento del árbol.
     */
     public void dfsInOrder(AccionVerticeArbolBinario<T> accion) {
         dfsInOrderaux(accion, raiz);

            }
    private void dfsInOrderaux(AccionVerticeArbolBinario<T> accion, Vertice a){
        if(a==null)
            return;
        if(a!=null){
            dfsInOrderaux(accion, a.izquierdo);
            accion.actua(a);
            dfsInOrderaux(accion, a.derecho);
        }

    }
    /**
     * Realiza un recorrido DFS <em>post-order</em> en el árbol, ejecutando la
     * acción recibida en cada elemento del árbol.
     * @param accion la acción a realizar en cada elemento del árbol.
     */
   public void dfsPostOrder(AccionVerticeArbolBinario<T> accion){
        dfsPostOrderaux(accion, raiz);
    }

     private void dfsPostOrderaux(AccionVerticeArbolBinario<T> accion, Vertice a){
        if(a==null)
            return;
        if(a!=null){
             dfsPostOrderaux(accion, a.izquierdo);
             dfsPostOrderaux(accion, a.derecho);
            accion.actua(a);
        }
    }
    /**
     * Regresa un iterador para iterar el árbol. El árbol se itera en orden.
     * @return un iterador para iterar el árbol.
     */
    @Override public Iterator<T> iterator() {
        return new Iterador();
    }
}
