package edd;

/**
 * Clase para colas genéricas.
 */
public class Cola<T> extends MeteSaca<T> {

    /**
     * Regresa una representación en cadena de la cola.
     * @return una representación en cadena de la cola.
     */
    @Override public String toString() {
        String s = "";
        if(!esVacia()){
            Nodo aux = cabeza;
            while(aux!=null){
                s+=aux.elemento.toString()+ ",";
                aux=aux.siguiente;
            }
        }
        return s;
    }

    /**
     * Agrega un elemento al final de la cola.
     * @param elemento el elemento a agregar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    @Override public void mete(T elemento) {
        Nodo nuevo = new Nodo(elemento);
        if (elemento ==null){
            throw new IllegalArgumentException ("Elemento nulo");
        }else{
            if(rabo == null){
        cabeza = rabo = nuevo;
        }else{
            rabo.siguiente = nuevo;
             rabo = nuevo;

         }

        }
    }
}
