package edd;
import java.util.NoSuchElementException;

import java.util.Iterator;

/**
 * <p>Clase para árboles binarios completos.</p>
 *
 * <p>Un árbol binario completo agrega y elimina elementos de tal forma que el
 * árbol siempre es lo más cercano posible a estar lleno.</p>
 */
public class ArbolBinarioCompleto<T> extends ArbolBinario<T> {

    /* Clase privada para iteradores de árboles binarios completos. */
    private class Iterador implements Iterator<T> {

        /* Cola para recorrer los vértices en BFS. */
        private Cola<Vertice> cola;

        /* Constructor que recibe la raíz del árbol. */
        public Iterador() {
            cola = new Cola<Vertice>();
            if(raiz!= null)
                cola.mete(raiz);
        }

        /* Nos dice si hay un elemento siguiente. */
        @Override public boolean hasNext() {
            return !cola.esVacia();
        }

        /* Regresa el siguiente elemento en orden BFS. */
        @Override public T next() {
            if (cola.esVacia())
                throw new NoSuchElementException("Mal");
            Vertice v = cola.saca();
            if(v.izquierdo != null)
                cola.mete(v.izquierdo);
            if(v.derecho != null)
                cola.mete(v.derecho);
            return v.elemento;
        }
    }

    /**
     * Constructor sin parámetros. Para no perder el constructor sin parámetros
     * de {@link ArbolBinario}.
     */
    public ArbolBinarioCompleto() { super(); }

    /**
     * Construye un árbol binario completo a partir de una colección. El árbol
     * binario completo tiene los mismos elementos que la colección recibida.
     * @param coleccion la colección a partir de la cual creamos el árbol
     *        binario completo.
     */
    public ArbolBinarioCompleto(Coleccion<T> coleccion) {
        super(coleccion);
    }
    /**
     * Agrega un elemento al árbol binario completo. El nuevo elemento se coloca
     * a la derecha del último nivel, o a la izquierda de un nuevo nivel.
     * @param elemento el elemento a agregar al árbol.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
   @Override public void agrega(T elemento) {
      Cola<Vertice> cola = new Cola<Vertice>();
         Vertice v = nuevoVertice(elemento);
          if(elemento == null){
            throw new IllegalArgumentException();
          }
        if(raiz == null)
            raiz = v;
        else {
            cola.mete(raiz);
            while(!cola.esVacia())
            {
              Vertice v1 = cola.saca();
            if(v1.hayIzquierdo()){
                cola.mete(v1.izquierdo);
            }
            if(v1.hayDerecho()) {
                cola.mete(v1.derecho);
            }
                if (v1.hayIzquierdo() && v1.hayDerecho()){ 
                    v1.derecho = v;
                    v.padre = v1;
                    break;
                }
                else if (!v1.hayIzquierdo() && !v1.hayDerecho()){
                    v1.izquierdo = v;
                    v.padre = v1;
                    break;
                }
            }
        }
        elementos++;
}
    /**
     * Elimina un elemento del árbol. El elemento a eliminar cambia lugares con
     * el último elemento del árbol al recorrerlo por BFS, y entonces es
     * eliminado.
     * @param elemento el elemento a eliminar.
     */
    @Override public void elimina(T elemento) {
        Vertice l = ultimoElemento();
        Vertice e = busquedaBFS(elemento);
        
        if (e != null) {
            elementos--;
            if (l == e){
                if (e == raiz) {
                    raiz = null;
                }
                else {
                    if (l.padre.izquierdo == l)
                        l.padre.izquierdo = null;
                    else
                        l.padre.derecho = null;
                }
            } 
            else {
                e.elemento = l.elemento;
                if (l.padre.izquierdo == l)
                    l.padre.izquierdo = null;
                else
                    l.padre.derecho = null;
            }
        }

     }

     private Vertice busquedaBFS(T e){
        Vertice r = null;
        Cola<Vertice> cola = new Cola<Vertice>();
        if (raiz == null)
            return r;
        
        else{
            cola.mete(raiz);
            while(!cola.esVacia()) {
              Vertice c = cola.saca();
                
                if(c.elemento.equals(e))  {
                    r = c;
                    break;
                }
                
                if(c.hayIzquierdo()) {
                    cola.mete(c.izquierdo);
                }
                if(c.hayDerecho()){
                    cola.mete(c.derecho);
                }
               
            }
        }
        
        return r;
        
    }



     private Vertice ultimoElemento(){
       Vertice l = null;
        Cola<Vertice> cola = new Cola<Vertice>();
        if (raiz == null)
            return l;
         else {
            cola.mete(raiz);
            while(!cola.esVacia()) {
                Vertice c = cola.saca();
                
                if(c.hayIzquierdo()) {
                    cola.mete(c.izquierdo);
                }
                if(c.hayDerecho()){
                    cola.mete(c.derecho);
                }
                if (!c.hayIzquierdo() && !c.hayDerecho() && cola.esVacia()) {
                    l = c;
                    break;
                }
            }
        }
        
        return l;
    }    
        

    /**
     * Regresa la altura del árbol. La altura de un árbol binario completo
     * siempre es ⌊log<sub>2</sub><em>n</em>⌋.
     * @return la altura del árbol.
     */
    @Override public int altura() {
        if(raiz==null)
            return -1;
        return (int) Math.floor(Math.log10(elementos)/Math.log10(2));
    }

    /**
     * Regresa un iterador para iterar el árbol. El árbol se itera en orden BFS.
     * @return un iterador para iterar el árbol.
     */
    @Override public Iterator<T> iterator() {
        return new Iterador();
    }
}
