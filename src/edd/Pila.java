package edd;

/**
 * Clase para pilas genéricas.
 */
public class Pila<T> extends MeteSaca<T> {

    /**
     * Regresa una representación en cadena de la pila.
     * @return una representación en cadena de la pila.
     */
    @Override public String toString() {
         String s = "";
        if(!esVacia()){
            Nodo nuevo = cabeza;
            while(nuevo!=null){
                s+=nuevo.elemento.toString() + "\n";
                nuevo=nuevo.siguiente;
            }
        }
        return s;
        

    }

    /**
     * Agrega un elemento al tope de la pila.
     * @param elemento el elemento a agregar.
     * @throws IllegalArgumentException si <code>elemento</code> es
     *         <code>null</code>.
     */
    @Override public void mete(T elemento) {
        if(elemento==null)
            throw new IllegalArgumentException("Elemento es nulo");
        Nodo nuevo = new Nodo(elemento);
        if(cabeza==null){
            cabeza=rabo=nuevo;
        }else{
            nuevo.siguiente = cabeza;
            cabeza = nuevo;
        } 
    }
    
}
